+++
title = "Typume"
description = "Programmable resume written in Typst with Functional-Typst."
weight = 3

[extra]
local_image = "/projects/resume.jpg"
link_to = "https://codeberg.org/terse/typume"
+++
