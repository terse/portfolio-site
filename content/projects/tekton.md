+++
title = "Tekton"
description = "Minimalist library for writing HTML pages in Haskell; future static site generator."
weight = 2

[extra]
local_image = "/projects/design.jpg"
link_to = "https://codeberg.org/terse/tekton"
+++
