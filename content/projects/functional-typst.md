+++
title = "Functional Typist"
description = "Functional extensions for Typst, a typsetting langauge for the modern age."
weight = 4

[extra]
local_image = "/projects/typewriter.jpg"
link_to = "https://codeberg.org/terse/functional-typst"
+++
