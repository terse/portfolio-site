+++
title = "About me"
path = "about"
+++

Welcome to `tersedev`!

I'm James, a software developer interested in functional programming patterns and how to write clear and concise (in short, terse) software.

I first got into coding with Ruby, and have since been learning functional programming in Ocaml and Haskell, along with some JavaScript/TypeScript, Python, and Janet.

You can find the projects I'm working on by going to my [projects page](/projects).
